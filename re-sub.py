import re


def solve_re(s):
    def _bool_op(m):
        maps = {
            "&&": "and",
            "||": "or",
        }
        return maps[m.group(0)]

    p = r"(?<= )((&&)|(\|\|))(?= )"
    res = re.sub(p, _bool_op, s)
    return res


solve = solve_re

assert solve("test && test") == "test and test"
assert solve("test && && test") == "test and and test"
assert solve("test || test") == "test or test"
assert solve("test||test") == "test||test"
assert solve("test&&test") == "test&&test"
assert solve("testTEST") == "testTEST"

n = int(input())
input_ = []
for _ in range(0, n):
    input_.append(input())

res = solve("\n".join(input_))
print(res)
