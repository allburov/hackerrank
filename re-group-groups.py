import re


def solve(n):
    p = r"^.*?([a-zA-Z0-9])\1"
    m = re.match(p, n)
    if m:
        return m.group(1)
    else:
        return "-1"


assert solve("...123") == "-1"
assert solve("...abc") == "-1"
assert solve("...aaab") == "a"
assert solve("..12345678910111213141516171820212223") == "1"
assert solve("4444") == "4"
assert solve("1234444") == "4"
assert solve("1234444123") == "4"
assert solve("...1234444123") == "4"
assert solve("...11234444123") == "1"

print(solve(input()))
