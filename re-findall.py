import re

regex_pattern = r"[qwrtypsdfghjklzxcvbnm]([aeiou]{2,})(?=[qwrtypsdfghjklzxcvbnm])"

NOT_VALID = ["-1"]


def solve(n):
    a = re.findall(regex_pattern, n, flags=re.IGNORECASE) or NOT_VALID
    # print(a)
    return a


assert solve("faaaafeeef") == ["aaaa", "eee"]
assert solve("aaaf") == NOT_VALID
assert solve("faaaf") == ["aaa"]
print("\n".join(solve(input())))
