import re


def solve(s):
    p = r"(?<=[:|,|: |, ])(#[0-9A-F]{3,6})"

    res = re.findall(p, s, flags=re.IGNORECASE | re.MULTILINE | re.DOTALL)
    return res


assert (
    solve(
        """
{
    color: #FfFdF8; background-color:#aef;
    font-size: 123px;
    background: -webkit-linear-gradient(top, #f9f9f9, #fff);
}
#Cab
{
    background-color: #ABC;
    border: 2px dashed #fff;
}   
"""
    )
    == ["#FfFdF8", "#aef", "#f9f9f9", "#fff", "#ABC", "#fff"]
)
assert solve(""" """) == []

n = int(input())
input_ = []
for _ in range(0, n):
    input_.append(input())

res = solve("\n".join(input_))
print("\n".join(res))
