import re


def solve(s):
    p = r"<([a-zA-Z][a-zA-Z0-9_\-.]+@[a-zA-Z]+\.[a-zA-Z]{1,3})>"

    if re.findall(p, s):
        return s
    return None


assert solve("DEXTER <dexter@hotmail.com>") == "DEXTER <dexter@hotmail.com>"
assert solve("VIRUS <virus!@variable.:p>") is None

n = int(input())
input_ = []
for _ in range(0, n):
    input_.append(input())

res = filter(bool, map(solve, input_))
print("\n".join(res))
