import re


class HTMLFinder:
    def __init__(self):
        self.result = []

    def feed(self, html):
        p = r"<(?P<tag>[a-zA-Z\d]+)[ ]{0,1}(?P<property>.*?)>(?P<body>.*?)</\1>|<(?P<tag1>[a-zA-Z\d]+)[ ]{0,1}(?P<property1>.*?)[ ]?/>"
        results_all = re.findall(p, html, flags=re.DOTALL)
        results = re.finditer(p, html, flags=re.DOTALL)
        for res in results:
            groups = res.groupdict()
            if groups.get("tag", False):
                tag = groups["tag"]
                attr = self._parse_attr(groups["property"])
                body = groups["body"]

                self.handle_starttag(tag, attr)
                self.feed(body)
                self.handle_endtag(tag)
            else:
                tag = groups["tag1"]
                attr = self._parse_attr(groups["property1"])
                self.handle_startendtag(tag, attr)

    def _parse_attr(self, s):
        attrs = {}
        if not s:
            return attrs

        # TODO: Поправить тут, более интелектуальный способ разденеия свойств
        p = r"(?P<name>[a-zA-Z-]+)=[\"'](?P<value>.*?)[\"']|(?P<name1>[a-zA-Z-]+)"
        for res in re.finditer(p, s):
            groups = res.groupdict()
            if groups.get("name", False):
                attrs[groups["name"]] = groups["value"]
            else:
                attrs[groups["name1"]] = None
        return attrs

    def handle_starttag(self, tag, attrs):
        self.result.append(f"Start : {tag}")
        self._print_attrs(attrs)

    def _print_attrs(self, attrs):
        for key, value in attrs.items():
            self.result.append(f"-> {key} > {value}")

    def handle_endtag(self, tag):
        self.result.append(f"End   : {tag}")

    def handle_startendtag(self, tag, attrs):
        self.result.append(f"Empty : {tag}")
        self._print_attrs(attrs)


def solve(s):
    s = ignore_comment(s)
    f = HTMLFinder()
    f.feed(s)
    return f.result


def ignore_comment(s):
    p = r"<!--.*?-->"
    no_comment = re.sub(p, "", s, flags=re.DOTALL)
    return no_comment


# solve('<script type="text/javascript" src="js/TitilliumMaps.font.js"></script><h1>Business Solutions</h1><br />')

solve(
    """
      
    
      <div class="entry-content">
          <p>...article text...</p>
          <p>...article text...</p>
    
          <figure>
            <img src="tammy-strobel.jpg" alt="Portrait of Tammy Strobel" />
            <figcaption>Tammy Strobel in her pared-down, 400sq-ft apt.</figcaption>
          </figure>
    
          <p>...article text...</p>
          <p>...article text...</p>
    
          <aside>
            <h2>Share this Article</h2>
            <ul>
              <li>Facebook</li>
              <li>Twitter</li>
              <li>Etc</li>
            </ul>
          </aside>
    
          <div class="entry-content-asset">
            <a href="photo-full.png">
              <img src="photo.png" alt="The objects Tammy removed from her life after moving" />
            </a>
          </div>
    
          <p>...article text...</p>
          <p>...article text...</p>
    
          <a class="entry-unrelated" href="http://fake.site/">Find Great Vacations</a>
      </div>
    
      """
)


def test_case_2():
    assert (
        (
            "\n".join(
                solve(
                    """<article class="hentry">
      <!-- <header>
        <h1 class="entry-title">But Will It Make You Happy?</h1>
        <time class="updated" datetime="2010-08-07 11:11:03-0400">08-07-2010</time>
        <p class="byline author vcard">
            By <span class="fn">Stephanie Rosenbloom</span>
        </p>
      </header> -->
    
      <div class="entry-content">
          <p>...article text...</p>
          <p>...article text...</p>
    
          <figure>
            <img src="tammy-strobel.jpg" alt="Portrait of Tammy Strobel" />
            <figcaption>Tammy Strobel in her pared-down, 400sq-ft apt.</figcaption>
          </figure>
    
          <p>...article text...</p>
          <p>...article text...</p>
    
          <aside>
            <h2>Share this Article</h2>
            <ul>
              <li>Facebook</li>
              <li>Twitter</li>
              <li>Etc</li>
            </ul>
          </aside>
    
          <div class="entry-content-asset">
            <a href="photo-full.png">
              <img src="photo.png" alt="The objects Tammy removed from her life after moving" />
            </a>
          </div>
    
          <p>...article text...</p>
          <p>...article text...</p>
    
          <a class="entry-unrelated" href="http://fake.site/">Find Great Vacations</a>
      </div>
    
      <footer>
        <p>
          A version of this article appeared in print on August 8,
          2010, on page BU1 of the New York edition.
        </p>
        <div class="source-org vcard copyright">
            Copyright 2010 <span class="org fn">The New York Times Company</span>
        </div>
      </footer>
    </article>"""
                )
            )
        )
        == """Start : article
-> class > hentry
Start : div
-> class > entry-content
Start : p
End   : p
Start : p
End   : p
Start : figure
Empty : img
-> src > tammy-strobel.jpg
-> alt > Portrait of Tammy Strobel
Start : figcaption
End   : figcaption
End   : figure
Start : p
End   : p
Start : p
End   : p
Start : aside
Start : h2
End   : h2
Start : ul
Start : li
End   : li
Start : li
End   : li
Start : li
End   : li
End   : ul
End   : aside
Start : div
-> class > entry-content-asset
Start : a
-> href > photo-full.png
Empty : img
-> src > photo.png
-> alt > The objects Tammy removed from her life after moving
End   : a
End   : div
Start : p
End   : p
Start : p
End   : p
Start : a
-> class > entry-unrelated
-> href > http://fake.site/
End   : a
End   : div
Start : footer
Start : p
End   : p
Start : div
-> class > source-org vcard copyright
Start : span
-> class > org fn
End   : span
End   : div
End   : footer
End   : article"""
    )


def test_case_1():
    assert (
        "\n".join(
            solve(
                """<script type="text/javascript" src="js/cufon-yui.js"></script>
        <script type="text/javascript" src="js/TitilliumMaps.font.js"></script><h1>Business Solutions</h1><br />
        <h2>Business Insurance</h2><script type="text/javascript">
            Cufon.replace('h1, h2', { fontFamily: "TitilliumMaps26L", hover: true });
        </script>"""
            )
        )
        == """Start : script
-> type > text/javascript
-> src > js/cufon-yui.js
End   : script
Start : script
-> type > text/javascript
-> src > js/TitilliumMaps.font.js
End   : script
Start : h1
End   : h1
Empty : br
Start : h2
End   : h2
Start : script
-> type > text/javascript
End   : script"""
    )


def test_case_5():
    assert (
        (
            "\n".join(
                solve(
                    """<!--[if !IE 6]><!-->
          <link rel="stylesheet" type="text/css" media="screen, projection" href="REGULAR-STYLESHEET.css" />
        <!--<![endif]-->
        <!--[if gte IE 7]>
          <link rel="stylesheet" type="text/css" media="screen, projection" href="REGULAR-STYLESHEET.css" />
        <![endif]-->
        <!--[if lte IE 6]>
          <link rel="stylesheet" type="text/css" media="screen, projection" href="http://universal-ie6-css.googlecode.com/files/ie6.0.3.css" />
        <![endif]-->"""
                )
            )
        )
        == """Empty : link
-> rel > stylesheet
-> type > text/css
-> media > screen, projection
-> href > REGULAR-STYLESHEET.css"""
    )


def test_example():
    assert (
        "\n".join(
            solve(
                """
                            
                        <!-- Comments<test></test> 
                        
                        
                        -->
                            <html><head><title>HTML Parser - I</title></head>
                            <body data-modal-target class='1'><h1>HackerRank</h1><br /></body></html>
                            """
            )
        )
        == """Start : html
Start : head
Start : title
End   : title
End   : head
Start : body
-> data-modal-target > None
-> class > 1
Start : h1
End   : h1
Empty : br
End   : body
End   : html"""
    )


def test_case_3():
    assert (
        "\n".join(
            solve(
                """<meta http-equiv="refresh" content="5;url=http://example.com/" />"""
            )
        )
        == """Empty : meta
-> http-equiv > refresh
-> content > 5;url=http://example.com/"""
    )


if __name__ == "__main__":
    n = int(input())
    input_ = []
    for _ in range(0, n):
        input_.append(input())

    print("\n".join(solve("\n".join(input_))))
