import re


def solve(s, k):
    k_re = fr"(?={k})"
    m = re.search(k_re, s)
    if m is None:
        yield -1, -1
        return
    else:
        for m in re.finditer(k_re, s):
            a = m.start(), m.end() + len(k) - 1
            # print(a)
            yield a


assert list(solve("b", "a")) == [(-1, -1)]

assert list(solve("aaadaa", "aa")) == [
    (0, 1),
    (1, 2),
    (4, 5),
]

print("\n".join(map(str, solve(input(), input()))))
