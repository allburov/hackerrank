import re


def solve(n):
    p = r"^[-+]?\d*(\.\d+)$"
    if re.match(p, n) is None:
        return False
    try:
        float(n)
    except ValueError:
        return False
    return True


assert solve("4") is False
assert solve("4.0O0") is False
assert solve("-1.00")
assert solve("+4.54")
assert solve("SomeRandomStuff") is False
assert solve("+4.50")
assert solve("1.0")
assert solve(".5")
assert solve(".7")
assert solve(".4")
assert solve("+-4.5") is False

if __name__ == "__main__":
    _t = int(input())
    for _ in range(0, _t):
        _b = input()
        print(solve(_b))
